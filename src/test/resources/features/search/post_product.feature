Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario:Original
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed for apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed for mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he doesn not see the results

  Scenario Outline: Positive Scenario
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/<validProduct>"
    Then he sees the results displayed for "<validProduct>"
    Examples:
      |validProduct|
      |apple  |
      |mango  |
      |tofu   |
      |water  |


  Scenario: Negative Scenario
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he doesn not see the results