package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        for (int i = 0; i< then().extract().body().jsonPath().getList("$").size(); i++){
            String title = "title["+i+"]";
            restAssuredThat(response -> response.body(title,contains("mango")));  //in case the assertion isn't case sensitive, comment this and uncomment the next line
//            restAssuredThat(response -> response.body(title,containsStringIgnoringCase("mango")));
        }
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        restAssuredThat(response -> response.body("detail.error",equalTo(true)));
    }

    @When("he calls endpoint {string}{string}")
    public void heCallsEndpointwithValidProduct(String arg0, String validProduct) {
        SerenityRest.given().get(arg0+validProduct);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForValidProduct(String validProduct) {
        restAssuredThat(response -> response.statusCode(200));
        for (int i = 0; i< then().extract().body().jsonPath().getList("$").size(); i++){
            String title = "title["+i+"]";
            restAssuredThat(response -> response.body(title,containsString(validProduct)));  //in case the assertion isn't case sensitive, comment this and uncomment the next line
//            restAssuredThat(response -> response.body(title,containsStringIgnoringCase(validProduct)));
        }
    }
}
