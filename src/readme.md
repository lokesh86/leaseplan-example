As requested, I have fixed the SearchStepDefinition.java file assertions. Now the assertions are being performed correctly.

Also I have utilitised the same code and added 2 more scenarios named "Positive Scenario" & "Negative Scenario". As the name suggests, they are to handle positive and negative cases.

Also, the current assertions are case sensitive however, if you want them to not to be case sensitive, update the:
 SearchStepDefinitions-> comment line 30 and 50 & uncomment line 31 & 51.
  


